const Sequelize = require("sequelize");

class RecordModel {
  init = conn => {
    return conn.define(
      "BoltRecord",
      {
        BoltRecordId: {
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        Logitude: Sequelize.STRING,
        Latitude: Sequelize.STRING,
        WattHourPerMile: Sequelize.FLOAT,
        CityPercent: Sequelize.FLOAT,
        HighwayPercent: Sequelize.FLOAT,
        IdlePercent: Sequelize.FLOAT,
        TripDistance: Sequelize.FLOAT,
        TripTime: Sequelize.FLOAT,
        BatteryLevel: Sequelize.FLOAT,
        CreatedDate: Sequelize.DATE,
        MilesPerHour: Sequelize.STRING
      },
      {
        timestamps: false,
        createdAt: false,
        updatedAt: false,
        freezeTableName: true,
        tableName: "BoltRecord"
      }
    );
  };
}

module.exports = RecordModel;
