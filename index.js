import express from "express";
var cors = require("cors");
const https = require("https");
const fs = require("fs");

const bodyParser = require("body-parser");
const app = express();

const port = 3008;
const SeqCon = require("./services/sequelizeConnection");
const RecordModel = require("./models/ObdDB");
let credentials = null;
if (process.env.ENVIRONMENT === "PROD") {
  const privateKey = fs.readFileSync(
    "/etc/letsencrypt/live/moosen.im/privkey.pem",
    "utf8"
  );
  const certificate = fs.readFileSync(
    "/etc/letsencrypt/live/moosen.im/cert.pem",
    "utf8"
  );
  const ca = fs.readFileSync(
    "/etc/letsencrypt/live/moosen.im/chain.pem",
    "utf8"
  );

  credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
  };
}

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Content-Type", "Application/json");
  next();
});
app.use(cors({ credentials: true, origin: "*" }));


const record = new RecordModel().init(SeqCon);

app.get("/bolt", (req, res) => {
  res.status(200);
  let lat = "[]";
  let log = "[]";
  let mph = "[]";
  try {
    log = req.query.kff1005.toString() || null;
    lat = req.query.kff1006.toString() || null;
    mph = req.query.kff1001.tostring() || null;

  } catch (e) {}
	console.log(req.query.kff1001);
  record.create({
    Logitude: log,
    Latitude: lat,
    WattHourPerMile: req.query.ke3f60e,
    BatteryLevel: req.query.k228334,
    CityPercent: req.query.kff1296,
    HighwayPercent: req.query.kff1297,
    IdlePercent: req.query.kff1298,
    TripDistance: req.query.kff120c,
    TripTime: req.query.kff1266,
    MilesPerHour: mph
  });
  res.send("OK!");
});

app.get("/viewAll", (req, res) => {
  record.findAll().then(r => {
    res.send(r);
  });
});

app.get("/tesla", (req, res) => {
  res.status(200);
  console.log(`Tesla query: ${req.query}`);
  let lat = "[]";
  let log = "[]";
  let mph = "[]";
  try {
    log = req.query.kff1005[0].toString() || null;
    lat = req.query.kff1006[0].toString() || null;
  } catch (e) {}


  res.send("OK!");
});
//init sql connection
SeqCon.authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

app.listen(port - 1, () => {
  console.log(`listening on port ${port - 1}`);
});

if (process.env.ENVIRONMENT === "PROD") {
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(port, () => {
    console.log(`HTTPS Server running on port ${port}`);
  });
}

app.on("listening", function() {
  console.log("OBD Reader online");
});
