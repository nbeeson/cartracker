const mysql = require("mysql2");
const dotenv = require("dotenv");
dotenv.config();

console.log(process.env.MYSQL_USER);
export const connection = mysql.createPool({
  host: process.env.MYSQL_ADDRESS,
  port: 3006,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  connectionLimit: 100,
  queueLimit: 0,
  socketPath: '/var/run/mysqld/mysqld.sock'
});

module.exports = connection;
