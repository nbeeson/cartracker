const crypto2 = require("crypto");
const sessions = [];
class EncryptionTools {
  genRandomString = length => {
    return crypto2
      .randomBytes(Math.ceil(length / 2))
      .toString("hex") /** convert to hexadecimal format */
      .slice(0, length); /** return required number of characters */
  };
  sha512 = (password, salt) => {
    let hash = crypto2.createHmac(
      "sha512",
      salt
    ); /** Hashing algorithm sha512 */
    hash.update(password);
    let value = hash.digest("hex");
    return {
      salt: salt,
      passwordHash: value
    };
  };
  saltHashPassword = (userpassword, userSalt) => {
    if (!userSalt) {
      let salt = this.genRandomString(16); /** Gives us salt of length 16 */
      let passwordData = this.sha512(userpassword, salt);
      return passwordData;
    } else {
      let passwordData = this.sha512(userpassword, userSalt);
      return passwordData;
    }
  };
  addToSessions = req => {
    try{
      sessions.push({
        Id: req.user.dataValues.CustomerId,
        SessionId: req.sessionID
      });
    }
    catch(e){
      console.log(e)
    }

  };
  ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    } else {
      console.log(req.user);
      res.json(`{"Error": "Authentication failed"}`);
    }
  };
  ensureTokenAuthenticated = (req, res, next) => {
    let ret = 0;
    if (req.headers["x-api-key"] != null) {
      console.log("req.user ", req.user);
      var checker = {
        Id: 1,
        SessionId: req.headers["x-api-key"]
      };
      console.log(checker);
      sessions.forEach(i => {
        if (i.Id === checker.Id) {
          if (i.SessionId == checker.SessionId) {
            ret = 1;
            console.log("found match")
            return next();
          }
        }
      });
    }
    if(ret === 0){
//    res.status(401);
    res.json(`{"Error": "Authentication failed"}`);
    }

  };
  getCustomerId = req => {
try{
	console.log(req.user)
    return req.user[0].dataValues.CustomerId || 0;
}
catch(e)
{console.log(req);
 return 0;
 }
  };
}
module.exports = EncryptionTools;
